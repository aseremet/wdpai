<?php

require 'Routing.php';

$names = [
    'ebooks' => 'e-booki',
    'calculator' => 'kalkulator',
    'blog' => 'blog',
    'contact' => 'kontakt',
    'cart' => 'koszyk',
    'aboutyou' => 'twoje-dane',
    'settings' => 'ustawienia',
    'login' => 'logowanie',
    'home' => '',
];

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url($path, PHP_URL_PATH);
$index = array_search($path, $names);

Routing::get('ebooks', 'DefaultController');
Routing::get('calculator', 'DefaultController');
Routing::get('blog', 'DefaultController');
Routing::get('contact', 'DefaultController');
Routing::get('cart', 'DefaultController');
Routing::get('aboutyou', 'DefaultController');
Routing::get('settings', 'DefaultController');
Routing::get('login', 'DefaultController');
Routing::get('home', 'DefaultController');
Routing::get('nomatch', 'DefaultController');

Routing::run($index);