<?php

require_once 'AppController.php';

class DefaultController extends AppController {
    public function ebooks() {
        $this->render('ebooks');
    }
    public function calculator() {
        $this->render('calculator');
    }
    public function blog() {
        $this->render('blog');
    }
    public function contact() {
        $this->render('contact');
    }
    public function cart() {
        $this->render('cart');
    }
    public function aboutyou() {
        $this->render('aboutyou');
    }
    public function settings() {
        $this->render('settings');
    }
    public function login() {
        $this->render('login');
    }
    public function home() {
        $this->render('home');
    }
    public function nomatch() {
        $this->render('nomatch');
    }
}